import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SiteHomeComponent } from './site/site-home.component';
import { MenuComponent } from './site/menu.component';
import { MenuItemComponent } from './site/menu-item.component';
import { UserAuthComponent } from './site/user-auth.component';
import { SharedModule } from './shared/shared.module';
import { CalendarComponent } from './shared/calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    SiteHomeComponent,
    MenuComponent,
    MenuItemComponent,
    UserAuthComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
