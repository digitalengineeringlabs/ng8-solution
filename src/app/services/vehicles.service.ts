export class VehiclesService{
    vehicles = [
        {
            id: '1',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 7
        },
        {
            id: '2',
            model: 'Audi A4',
            type: 'Car',
            rented: false,
            color: 'white',
            seats: 5
        },
        {
            id: '3',
            model: 'Mini cooper',
            type: 'Car',
            rented: false,
            color: 'blue',
            seats: 2
        }
    ]

    getVehicles(){
        return this.vehicles;
    }

    updateRentStatus(data) {
        console.log('service changing data ',data);
        this.vehicles.forEach(vh => {
            if(vh.id === data.id) {
                vh.rented = data.rented
            }
        })
    }

}