import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import {HttpClient} from '@angular/common/http';

export class UserService{
    items = ['Tomato','Carrot'];

    constructor(private http:HttpClient){

    }

    getItems() {
        return Observable.create((observer:Observer<any>)=>{
            this.http.get("https://jsonplaceholder.typicode.com/users").subscribe((data)=>{
                observer.next(data);
            })
        })

        // return new Promise((resolve, reject)=>{
        //     this.http.get("https://jsonplaceholder.typicode.com/users").subscribe((data)=>{
        //         resolve(data);
        //     }, (error) => {
        //         reject(error);
        //     })
        // });
        
    }
}


