import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CustomersService {
    name:String = 'Mike';

    getName(){
        return this.name;
    }
}