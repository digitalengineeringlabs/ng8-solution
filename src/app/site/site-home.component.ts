import { Component } from '@angular/core';

@Component({
    selector: 'app-site-home',
    template: `<p>Site Home</p>
    <app-user-auth></app-user-auth>
    <app-calendar></app-calendar>
    `
})
export class SiteHomeComponent {

}