import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { log } from 'util';

@Component({
    selector: 'app-user-auth',
    template: `
        <div class='user-auth'>
            <span>Welcome Mike</span>
            <app-menu></app-menu>
            <ul>
                <li *ngFor="let v of users">{{v.name}}</li>
            </ul>
        </div>
    `,
    styles: [
        `
        .user-auth{
            background-color: #ddd;
        }
        `
    ],
    providers: [UserService]
})
export class UserAuthComponent {

    users = [];

    constructor(private userSvc:UserService){
        console.log('Constructor called');
      }
    
      ngOnInit(){
        this.userSvc.getItems().subscribe((data:any[])=>{
            console.log('recieved data ',data);
            this.users =  data;
        },
        (error)=>{
            console.log('recieved error '+error);
        });

        // this.userSvc.getItems()
        // .then((data:any[])=>{
        //     console.log('recieved data ',data);
        //     this.users =data;
        // })
        // .catch((error)=>{
        //     console.log(error);
        // });
        console.log(this.userSvc.getItems());
      }
}