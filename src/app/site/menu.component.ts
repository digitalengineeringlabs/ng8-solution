import { Component } from '@angular/core';

@Component({
    selector: 'app-menu',
    template: `
    <ul>
        <li>My Profile</li>
        <app-menu-item></app-menu-item>
        <li>Log Out</li>
    </ul>
    `
})
export class MenuComponent {
}