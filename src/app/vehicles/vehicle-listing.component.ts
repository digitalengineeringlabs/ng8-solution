import { Component } from '@angular/core';
import { VehiclesService } from '../services/vehicles.service';

@Component({
    selector: 'app-vehicle-listing',
    template: `
        <app-vehicle-add (added)="add($event)"></app-vehicle-add>
        <app-vehicle-book *ngFor="let vh of vehicles"
            [vehicle]="vh"
            (bookAction)="handleBooking($event)"
            [admin]="adminUser"
            (removeVehicle)="remove($event)"></app-vehicle-book>
    `
})
export class VehicleListingComponent {
    adminUser = true;
    vehicles = [];

    constructor(private vhSvc:VehiclesService){
        console.log('constructor called');
        console.log(this.vhSvc.vehicles);
    }

    ngOnInit(){
        this.vehicles = this.vhSvc.getVehicles();
    }

    handleBooking(data) {
        console.log('Data ',data);
        this.vhSvc.updateRentStatus(data);
    }

    remove(data) {
        const index = this.vhSvc.getVehicles().findIndex(v => v.id === data.id);
        this.vhSvc.getVehicles().splice(index, 1);
        console.log('Removed '+data.id);
    }

    add(data) {
        this.vhSvc.getVehicles().push(data);
        console.log('Added '+data.id);
    }

}