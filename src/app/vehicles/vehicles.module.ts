import { NgModule } from '@angular/core';
import { VehiclesComponent } from './vehicles.component';
import { VehicleBookComponent } from './vehicle-book.component';
import { VehicleAddComponent } from './vehicle-add.component';
import { VehicleListingComponent } from './vehicle-listing.component';
import { VehiclesService } from '../services/vehicles.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VehiclesRoutingModule } from './vehicles-routing.module';

@NgModule({
    declarations:[
        VehiclesComponent,
        VehicleBookComponent,
        VehicleAddComponent,
        VehicleListingComponent
    ],
    imports:[
        FormsModule,
        CommonModule,
        VehiclesRoutingModule
    ],
    providers:[VehiclesService]
})
export class VehiclesModule{

}