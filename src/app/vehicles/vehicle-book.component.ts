import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-vehicle-book',
    template: `
    <div [ngStyle]="{'padding':'10px', 'margin-top':'10px', 'font-family':'Verdana'}" 
    [ngClass]="{'available': !vehicle.rented, 'notAvailable': vehicle.rented}">
        <h3 [ngStyle]="{'margin':'0'}">{{vehicle.model}}</h3>
        <p>{{vehicle.color}} {{vehicle.type}} with {{vehicle.seats}} seats</p>
        <div *ngIf="vehicle.rented"><button (click)="cancelBook(vehicle.id)">Cancel Booking</button></div>
        <button [hidden]="vehicle.rented" (click)="bookNow(vehicle.id)">Book Now</button>
        <div *ngIf="admin"><button (click)="remove(vehicle.id)">Remove</button></div>
    </div>
    `,
    styles: [`
        .available {
            color: white;
            background-color: green;
        }
        .notAvailable {
            color: yellow;
            background-color: #c66;
        }
    `]
})
export class VehicleBookComponent{

    @Input() admin;
    @Input() vehicle;
    @Output() bookAction = new EventEmitter();
    @Output() removeVehicle = new EventEmitter();

    bookNow(id){
        console.log('Book Now', id);        
        this.bookAction.emit({id:id, rented: true});
    }

    cancelBook(id){
        console.log('Cancel Booking', id);
        this.bookAction.emit({id:id, rented: false});
    }

    remove(id) {
        this.removeVehicle.emit({id:id});
    }
}