import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-vehicle-add',
    template: `
        <div>
            <p>Model: <select [(ngModel)]="info.model">
                    <option value=''>Select</option>
                    <option>BMW X3</option>
                    <option>Audi A2</option>
            </select></p>
            <p>Type: <input type='radio' name='type' value='SUV' [(ngModel)]="info.type">SUV
                    <input type='radio' name='type' value='Car' [(ngModel)]="info.type">Car</p>
            <p>Color: <input type='color' [(ngModel)]="info.color"></p>
            <p>Seats: <input type='text' [(ngModel)]="info.seats"></p>
            <button (click)="submitChanges()">Submit</button>
        </div>
        <br/>
        <div>
            Model: {{this.info.model}}<br/>
            Type: {{this.info.type}}<br/>
            Color: {{this.info.color}}<br/>
            Seats: {{this.info.seats}}
        </div>
    `
})
export class VehicleAddComponent {
    info = {
        id: 0,
        model: '',
        type: '',
        rented: false,
        color: '',
        seats: 0
    }
    @Output() added = new EventEmitter();

    submitChanges() {
        console.log('Model: '+this.info.model);
        console.log('Type: '+this.info.type);
        console.log('Color: '+this.info.color);
        console.log('Seats: '+this.info.seats);
        const randomeUniqueId = Math.round(Math.random()*1000);
        this.info.id = randomeUniqueId;
        this.added.emit(this.info);
    }
}