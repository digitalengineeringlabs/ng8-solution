import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicles',
    template: `
        <div [ngStyle]="{'padding':'10px', 'margin-top':'10px', 'font-family':'Verdana'}" 
            [ngClass]="{'available': !vh.rented, 'notAvailable': vh.rented}" 
            *ngFor="let vh of vehicles">
            <h3 [ngStyle]="{'margin':'0'}">{{vh.model}}</h3>
            <p>{{vh.color}} {{vh.type}} with {{vh.seats}} seats</p>
            <p>{{'$'}}{{vh.costPerKm}}/Km</p>
            
            <!-- <div *ngIf="vh.rented; else bookNow"><button (click)="preBookNow(vh.id)">Pre Book Now</button></div>
            Event is not handled from elements in ng-template
            <ng-template #bookNow><button (click)="bookNow(vh.id)">Book Now</button></ng-template> -->

            <div *ngIf="vh.rented"><button (click)="cancelBook(vh.id)">Cancel Booking</button></div>
            <button [hidden]="vh.rented" (click)="bookNow(vh.id)">Book Now</button>
        </div>
    `,
    styles: [`
        .available {
            color: white;
            background-color: green;
        }
        .notAvailable {
            color: yellow;
            background-color: #c66;
        }
    `]
})
export class VehiclesComponent {
    vehicles = [
        {
            id: '1',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 7,
            costPerKm: 5 
        },
        {
            id: '2',
            model: 'Audi A4',
            type: 'Car',
            rented: false,
            color: 'white',
            seats: 5,
            costPerKm: 6.5
        },
        {
            id: '3',
            model: 'Mini cooper',
            type: 'Car',
            rented: false,
            color: 'blue',
            seats: 2,
            costPerKm: 4
        }
    ]

    bookNow(id){
        console.log('Book Now', id);        
        this.vehicles.forEach(vh => {
            if(vh.id === id) {
                vh.rented = true
            }
        })
    }

    cancelBook(id){
        console.log('Cancel Booking', id);
        this.vehicles.forEach(vh => {
            if(vh.id === id) {
                vh.rented = false
            }
        })
    }
}