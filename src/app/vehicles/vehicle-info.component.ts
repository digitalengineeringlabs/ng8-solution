import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-info', 
    template: `
        <div>
            <h3>Model</h3>
            <p>Type, Seats</p>
            <p>Text, showing "Available" if rented is true, otherwise "Not Available"</p>
            <p>Color</p>
            // <h3>{{info.model}}</h3>
            // <p>{{info.type}}, Seats: {{info.seats}}</p>
            // <p>{{info.rented ? 'Not Available' : 'Available'}}</p>
            // <p>{{info.color}}</p>
        </div>
    `
}) 
export class VehicleInfoComponent {   
    info = {
            id: 'B001',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 5
        }
}