import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-edit',
    template: `
        <div>
        <p>Model: <select (change)="modelChanged($event)">
        <option value=''>Select</option>
        <option [selected]="isModel('BMW X3')">BMW X3</option>
        <option [selected]="isModel('Audi A2')">Audi A2</option>
</select></p>
<p>Type: <input type='radio' name='type' value='SUV' 
            (click)="typeChanged($event)" [checked]="isType('SUV')">SUV
        <input type='radio' name='type' value='Car'
            (click)="typeChanged($event)" [checked]="isType('Car')">Car</p>
<p>Color: <input type='color' 
            (change)="colorChanged($event)" [value]="info.color"></p>
<p>Seats: <input type='text' 
            (change)="seatChanged($event)" [value]="info.seats"></p>
<button (click)="submitChanges()">Submit</button>
        </div>
        <br/>
        <div>
        Model: {{this.info.model}}<br/>
        Type: {{this.info.type}}<br/>
        Color: {{this.info.color}}<br/>
        Seats: {{this.info.seats}}
        </div>
    `
})
export class VehicleEditComponent {
    info = {
        id: 'B001',
        model: 'Audi A2',
        type: 'Car',
        rented: true,
        color: '#226699',
        seats: 5
    }

    isType(type) {
        return type == this.info.type ? true : false;
    }

    isModel(model) {
        return model == this.info.model ? true : false;
    }

    modelChanged(e){
        this.info.model = e.target.value;
    }

    typeChanged(e){
        this.info.type = e.target.value;
    }

    colorChanged(e){
        this.info.color = e.target.value;
    }

    seatChanged(e){
        this.info.seats = e.target.value;
    }
    
    submitChanges() {
        console.log('Model: '+this.info.model);
        console.log('Type: '+this.info.type);
        console.log('Color: '+this.info.color);
        console.log('Seats: '+this.info.seats);
    }
}