# Topics
Test
## 1. Components

## 2. Data Binding

### String Interpolation

### Property Binding

### Event Binding

### Two-way Data Binding

## 3. Directives

### Attribute Directives

### Structural Directives

## 4. Custom Data Binding

### Custom Property Binding

### Custom Event Binding
